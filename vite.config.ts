import { defineConfig } from 'vite';

import react from '@vitejs/plugin-react-swc';
import { VitePWA } from 'vite-plugin-pwa';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    react(),
    VitePWA({
      strategies: 'injectManifest',
      srcDir: 'src/service-worker',
      filename: 'service-worker.ts',
      injectRegister: false,
      includeManifestIcons: false,
      manifest: {
        name: 'Hyper-Graph',
        short_name: 'Hyper-Graph',
        description: 'Social network of the future',
        theme_color: '#ced4da',
        icons: [
          {
            src: '/vite.svg',
            sizes: 'any',
          },
        ],
      },
      devOptions: {
        enabled: true,
        type: 'module',
      },
    }),
  ],
});
