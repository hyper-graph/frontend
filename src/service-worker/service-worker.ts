import { clientsClaim } from 'workbox-core';
import { precacheAndRoute } from 'workbox-precaching';

declare const self: ServiceWorkerGlobalScope;

precacheAndRoute(self.__WB_MANIFEST);

self.skipWaiting();
clientsClaim();

const authBroadcast = new BroadcastChannel('AuthEvent');

// setInterval(() => {
//   authBroadcast.postMessage({
//     eventName: 'AuthEvent',
//     message: 'Test message',
//   });
// }, 1000);

authBroadcast.addEventListener('message', () => {
  // console.log('from worker', message);
});
