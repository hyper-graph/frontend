type Data<T = unknown> = {
  eventName: string;
  data: T;
};

export class HGEvent<D extends Data = Data> {
  public readonly name: string;
  public readonly data: D['data'];
  public readonly receivedAt: Date;

  public static fromEvent<D extends Data = Data>(event: MessageEvent<D>): HGEvent<D> {
    return new HGEvent<D>(event.data);
  }

  public static fromData<D extends Data = Data>(data: D): HGEvent<D> {
    return new HGEvent<D>(data);
  }

  protected constructor(event: D) {
    this.receivedAt = new Date();
    this.data = event.data;
    this.name = event.eventName;
  }

  public getData(): { data: D['data']; eventName: string } {
    return {
      eventName: this.name,
      data: this.data,
    };
  }
}

export const isHGEventCandidate = (event: MessageEvent<unknown>): event is MessageEvent<Data> => {
  const { data } = event;

  return data !== null && typeof data === 'object' && 'eventName' in data && typeof data.eventName === 'string';
};
