import { HGEvent } from '../../event.ts';

type Params = {
  eventName: 'AuthEvent';
  data: string;
};

export class AuthEvent extends HGEvent<Params> {}
