import { v4 } from 'uuid';

import { HGEvent, isHGEventCandidate } from './event.ts';

type SubscriberId = string;

type EventCallback<Event extends HGEvent = HGEvent> = (data: Event) => void;

export class EventBus<Event extends HGEvent> {
  private subscribers = new Map<SubscriberId, EventCallback<Event>>();
  private readonly channel: BroadcastChannel;

  constructor(eventName: string) {
    this.channel = new BroadcastChannel(eventName);
    this.channel.addEventListener('message', (event: MessageEvent<Event>) => {
      if (isHGEventCandidate(event)) {
        this.publishEvent(HGEvent.fromEvent(event) as Event);
      }
    });
  }

  public subscribe(callback: EventCallback<Event>): () => void {
    const id = v4();
    this.subscribers.set(id, callback);

    return () => {
      this.subscribers.delete(id);
    };
  }

  public emitEvent(event: Event): void {
    this.channel.postMessage(event.getData());
  }

  private publishEvent(event: Event) {
    Array.from(this.subscribers.values()).forEach(callback => {
      callback(event);
    });
  }
}
