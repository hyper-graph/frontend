import styles from './auth.module.scss';

import { Form } from '../../component/form/form.view.tsx';

import { useAuthForm } from './auth.data.ts';

export const AuthView = () => {
  const { onSubmit, onInput } = useAuthForm();

  return (
    <div className={styles.main}>
      <h1>Auth Page</h1>

      <Form submitText="Register" onSubmit={onSubmit} onInput={onInput}>
        <Form.Input name="1" title="First name" />
        <Form.Input name="2" title="First name" />
        <Form.Input name="3" title="Username" />
        <Form.Input name="4" title="Password" type="password" />
      </Form>
    </div>
  );
};
