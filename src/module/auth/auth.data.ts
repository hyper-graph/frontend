import React, { useState } from 'react';

type AuthForm = {
  firstName: string;
  lastName: string;
  username: string;
  password: string;
};

export const useAuthForm = () => {
  const [formData, setFormData] = useState<AuthForm>({
    firstName: '',
    lastName: '',
    username: '',
    password: '',
  });

  const onSubmit = (event: React.FormEvent<HTMLFormElement>): void => {
    event.preventDefault();
    console.log(formData);
  };

  const onInput = ({ target: { name, value } }: React.ChangeEvent<HTMLFormElement>): void => {
    setFormData(data => ({
      ...data,
      [name]: value,
    }));
  };

  return {
    onSubmit,
    onInput,
  };
};
