import { useState } from 'react';

import { HGEvent } from '../event/event.ts';

type State<D> = {
  local: boolean;
  value: D;
};

type UseSharedInput<E extends HGEvent> = {
  inputData: State<E['data']>;
  setInputValue(value: State<E['data']>): void;
  eventCallback(event: E): void;
};

export const useSharedInput = <E extends HGEvent>(initialValue: E['data']): UseSharedInput<E> => {
  const [inputData, setInputValue] = useState<State<HGEvent['data']>>({ local: true, value: initialValue });

  function eventCallback(event: E) {
    const newValue = event.data;
    if (!inputData.local || inputData.value === '' || inputData.value === newValue) {
      setInputValue({ local: false, value: newValue });
    }
  }

  return {
    inputData,
    setInputValue,
    eventCallback,
  };
};
