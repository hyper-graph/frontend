import React, { HTMLInputTypeAttribute } from 'react';

type InputData = {
  name: string;
  title: string;
  type?: HTMLInputTypeAttribute;
};

type InputChildren = React.ReactElement<InputData> | React.ReactElement<InputData>[];

type FormParams = {
  children: InputChildren;
  submitText: string;
  onSubmit(e: React.FormEvent<HTMLFormElement>): void;
  onInput(e: React.ChangeEvent<HTMLFormElement>): void;
};

export const Form = ({ children, submitText, onInput, onSubmit }: FormParams): React.ReactElement => {
  return (
    <form className="flex flex-col gap-y-4" onSubmit={onSubmit} onInput={onInput}>
      {children}
      <button type="submit">{submitText}</button>
    </form>
  );
};

const Input = ({ name, title, type = 'text' }: InputData): React.ReactElement => {
  return (
    <label>
      <span>{title}</span>
      <input type={type} name={name} />
    </label>
  );
};

Form.Input = Input;
