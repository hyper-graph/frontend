import React from 'react';
import ReactDOM from 'react-dom/client';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-expect-error
import { registerSW } from 'virtual:pwa-register';

import './index.css';

import { AuthView } from './module/auth/auth.view';

const root = document.getElementById('root');

if (root) {
  ReactDOM.createRoot(root).render(
    <React.StrictMode>
      <AuthView />
    </React.StrictMode>
  );
}

window.addEventListener('load', () => {
  if ('serviceWorker' in navigator) {
    registerSW();
  }
});
