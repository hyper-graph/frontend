import { useEffect } from 'react';

import { EventBus } from '../event/event-bus.ts';
import { AuthEvent } from '../event/events/auth/auth-event.ts';
import { HGEvent } from '../event/event.ts';

type AuthEventHandler = (event: AuthEvent) => void | Promise<void>;

export const authEventBus = new EventBus<AuthEvent>(AuthEvent.name);

export const useAuthEvents = (callback: AuthEventHandler) => {
  useEffect(() => {
    const unsubscribe = authEventBus.subscribe(callback);
    return () => {
      unsubscribe();
    };
  }, [callback]);

  return {
    dispatch: (data: string) => {
      authEventBus.emitEvent(
        HGEvent.fromData({
          eventName: 'typing',
          data,
        })
      );
    },
  };
};
